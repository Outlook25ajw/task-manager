import React, { useContext, useState, useEffect } from 'react';
import {TaskListContext} from './context/TaskListContext';
import './App.css';

const TaskForm = () => {
    const {addTask, clearList, editItem, editTask} = useContext(TaskListContext);
    const [title, setTitle] = useState('')

    const handleChange = e => {
        setTitle(e.target.value);
    }

    const handleSubmit = e => {
        e.preventDefault();
        if(!editItem) {
            addTask(title);
            setTitle("");
        }
        else {
            editTask(title, editItem.id);
        }
    };

    useEffect(() => {
        if(editItem) {
            setTitle(editItem.title)
        } else {
            setTitle("")
        }
    }, [editItem])

    return (
        <form onSubmit={handleSubmit} className="form" autocomplete="off">
            <div className="container-4">
                <input onChange={handleChange} value={title} type="text" id="input" placeholder="Add task here" required />
            </div>
            <div className="buttons">
                <button type="submit" className="btn btn-3">{editItem ? 'Edit Task' : 'Create Task'}</button>
                <button onClick={clearList} className="btn btn-3">Clear</button>
            </div>
        </form>
    )
}


export default TaskForm;