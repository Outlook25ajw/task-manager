import React,{useContext} from 'react';
import {TaskListContext} from './context/TaskListContext';
import './App.css';

const Task = ({task}) => {
    const {removeTask, findItem} = useContext(TaskListContext);
    // const [show, toggleShow] = React.useState(false);

    return (
        <div className="task-main">
            <li className="task">
                <span>{task.title}</span>
                <div>
                    <button onClick={(id) => removeTask(task.id)} className="task-button"><i className="fas fa-trash-alt"></i></button>
                    <button onClick={() => findItem(task.id)} className="task-button-1"><i className="fas fa-pen"></i></button>
                </div>
            </li>
        </div>
    )
}

export default Task;