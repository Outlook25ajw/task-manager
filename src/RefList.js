import React, {useContext} from 'react'
import {RefListContext} from "./context/RefListContext";
import RefTask from "./RefTask";
import "./App.css"

const RefList = () => {
    const {links} = useContext(RefListContext);
    return (
        <div className="task-list">
            {links.length ? (
                <ul className="list">
                    {links.map((link) => {
                        return <RefTask link={link} key={link.id} />;
                    })}
                </ul>
            ) : (
                <div className="no-tasks">No Links ..</div>
            )}
        </div>
    )
}

export default RefList;