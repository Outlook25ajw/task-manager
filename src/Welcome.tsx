import React from 'react';
import {
  Button,
  Jumbotron } from 'reactstrap';
import moment from "moment"

interface WelcomeProps {
  isAuthenticated : boolean;
  authButtonMethod : any;
  user : any;
}

interface WelcomeState {
  isOpen : boolean;
}

function WelcomeContent(props: WelcomeProps) {
  // If authenticated, greet the user
  if (props.isAuthenticated) {
    return (
      <div>
        <h4 style={{textAlign:'center'}}>{props.user.displayName}'s tasks for  <span>{moment().format('MMMM Do YYYY')}</span> </h4>
      </div>
    );
  }

  // Not authenticated, present a sign in button
  return <Button color="primary" onClick={props.authButtonMethod}>Click here to sign in</Button>;
}

export default class Welcome extends React.Component<WelcomeProps, WelcomeState> {
  render() {
    return (
      <Jumbotron style={{backgroundColor:'bisque'}}>
        <WelcomeContent
          isAuthenticated={this.props.isAuthenticated}
          user={this.props.user}
          authButtonMethod={this.props.authButtonMethod} />
      </Jumbotron>
    );
  }
}