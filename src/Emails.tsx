import React from 'react';
import { Table } from 'reactstrap';
import moment from 'moment';
import { Message } from 'microsoft-graph';
import { config } from './Config';
import { getEmails } from './GraphService';
import withAuthProvider, { AuthComponentProps } from './AuthProvider';

interface EmailState {
  events: Message[];
}

class Emails extends React.Component<AuthComponentProps, EmailState> {
  constructor(props: any) {
    super(props);

    this.state = {
      events: [],
    };
  }

  async componentDidMount() {
    try {
      // Get the user's access token
      var accessToken = await this.props.getAccessToken(config.scopes);
      // Get the user's events
      var events = await getEmails(accessToken);
      // Update the array of events in state
      this.setState({events: events.value});
    }
    catch(err) {
      this.props.setError('ERROR', JSON.stringify(err));
    }
  }

  render() {
    console.log(this.state)
    return (
      <div>
        <h1>PR's to review</h1>
        <Table>
          <thead>
            <tr>
              <th scope="col">Sender</th>
              <th scope="col">Email</th>
              <th scope="col">Subject</th>
              <th scope="col">Sent</th>
            </tr>
          </thead>
  

<tbody>
{this.state.events.map(
  function(event: Message){
    return(
      <tr key={event.id}>
        <td style={{minWidth: '200px'}}>{event.sender?.emailAddress?.name}</td>
        <td style={{minWidth: '200px'}}>{event.subject}</td>
        <td>{event.bodyPreview}</td>
        <td style={{minWidth: '150px'}}>{moment.utc(event.sentDateTime).local().format('M/D/YY h:mm A')}</td>
      </tr>
    );
  })}
</tbody>
        </Table>
      </div>
    );
  }
}

export default withAuthProvider(Emails);