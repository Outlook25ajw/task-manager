import React, {createContext, useState, useEffect} from 'react';
import {v1 as uuid} from 'uuid'

export const RefListContext = createContext()

const RefListContextProvider = props => {

    const initialState = JSON.parse(localStorage.getItem('links')) || []

    const [links, setLinks] = useState(initialState)

    useEffect(() => {
        localStorage.setItem('links', JSON.stringify(links))
    },[links])

    const addLink = (title, link) => {
        setLinks([...links,{title, link, id: uuid()}])
    }

    const removeLink = id => {
        setLinks(links.filter(link => link.id !== id))
    }

    return (
        <RefListContext.Provider value={{links, addLink, removeLink}}>
            {props.children}
        </RefListContext.Provider>
    )
}

export default RefListContextProvider;