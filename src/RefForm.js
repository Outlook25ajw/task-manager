import React, { useContext, useState} from 'react';
import {RefListContext} from './context/RefListContext';
import './App.css';

const RefForm = () => {
    const {addLink} = useContext(RefListContext);
    const [title, setTitle] = useState('')
    const [link, setLinks] = useState('')


    // const handleChange = e => {
    //     setTitle(e.target.value);
    // }

    // const handleLinkChange = e => {
    //     setLink(e.target.value);
    // }

    const handleSubmit = e => {
        e.preventDefault();
        addLink(title, link); 
        setTitle('');
        setLinks('');
    };

    return (
        <form onSubmit={handleSubmit} className="form" autocomplete="off">
            <div className="container-5">
                <input onChange={(e) => setTitle(e.target.value)} value={title} type="text" id="input" placeholder="Add Subject" required />
                <input onChange={(e) => setLinks(e.target.value)} value={link} type="text" id="input" placeholder="Add Link" required />
            </div>
            <div className="buttons">
                <button type="submit" className="btn btn-3">{'Add Refrence'}</button>
            </div>
        </form>
    )
}


export default RefForm;