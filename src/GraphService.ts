var graph = require('@microsoft/microsoft-graph-client');

function getAuthenticatedClient(accessToken: string) {
  // Initialize Graph client
  const client = graph.Client.init({
    // Use the provided access token to authenticate
    // requests
    authProvider: (done: any) => {
      done(null, accessToken);
    }
  });

  return client;
}

export async function getUserDetails(accessToken: string) {
  const client = getAuthenticatedClient(accessToken);

  const user = await client.api('/me').get();
  return user;
}

export async function getEvents(accessToken: string) {
    const client = getAuthenticatedClient(accessToken);
  
    const events = await client
      .api('/me/events')
      .select('subject,organizer,start,end,location')
      .orderby('createdDateTime DESC')
      .get();
  
    return events;
  }


  export async function getEmails(accessToken: string) {
    const client = getAuthenticatedClient(accessToken);
  
    const emails = await client
      .api('me/messages?$search="subject:pr"')  
      .select('sender,subject,bodyPreview,sentDateTime')
	    .get();
  
    return emails;
  }