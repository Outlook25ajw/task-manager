import React,{useContext} from 'react';
import {RefListContext} from './context/RefListContext';
import './App.css';

const RefTask = ({link}) => {
    const {removeLink} = useContext(RefListContext);
    return (
        <div className="task-main">
            <li className="task">
                <div>
                <p>{link.title}</p>
                <a href={link.link}  target="_blank" rel="noopener noreferrer" >{link.link}</a>
                </div>
                <div>
                    <button onClick={() => removeLink(link.id)} className="task-button"><i className="fas fa-trash-alt"></i></button>
                </div>
            </li>
        </div>
    )
}

export default RefTask;