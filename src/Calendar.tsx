import React from 'react';
import { Table } from 'reactstrap';
import moment from 'moment';
import { Event } from 'microsoft-graph';
import { config } from './Config';
import { getEvents } from './GraphService';
import withAuthProvider, { AuthComponentProps } from './AuthProvider';

interface CalendarState {
  events: Event[];
}

// Helper function to format Graph date/time
function formatDateTime(dateTime: string | undefined) {
  if (dateTime !== undefined) {
    return moment.utc(dateTime).local().format('M/D/YY h:mm A');
  }
}

class Calendar extends React.Component<AuthComponentProps, CalendarState> {
  constructor(props: any) {
    super(props);

    this.state = {
      events: [],
    };
  }

  async componentDidMount() {
    try {
      // Get the user's access token
      var accessToken = await this.props.getAccessToken(config.scopes);
      // Get the user's events
      var events = await getEvents(accessToken);
      // Update the array of events in state
      this.setState({events: events.value});
    }
    catch(err) {
      this.props.setError('ERROR', JSON.stringify(err));
    }
  }

  render() {
    const curr = (moment.utc().local().format('M/D/YY'))
    console.log(this.props)
    console.log(this.state.events)
    return (
      <div>
        <h1>Meetings</h1>
        <Table>
          <thead>
            <tr>
              <th scope="col">Organizer</th>
              <th scope="col">Subject</th>
              <th scope="col">Location</th>
              <th scope="col">Start</th>
              <th scope="col">End</th>
            </tr>
          </thead>
  

<tbody>
{this.state.events.sort((a, b) => a > b ? 1 : -1).map(
  function(event: Event){
    return(
      <React.Fragment>
       { moment.utc(event.start?.dateTime).local().format('M/D/YY') === curr ?
      <tr key={event.id}>
        <td>{event.organizer?.emailAddress?.name}</td>
        <td>{event.subject}</td>
        <td>{event.location?.displayName}</td>
        <td>{formatDateTime(event.start?.dateTime)}</td>
        <td>{formatDateTime(event.end?.dateTime)}</td>
      </tr>
      :
      <div style={{display:'none'}}>None</div>
      }
       </React.Fragment>
    );
  })}
</tbody>
        </Table>
      </div>
    );
  }
}

export default withAuthProvider(Calendar);